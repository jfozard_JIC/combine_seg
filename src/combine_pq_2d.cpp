
#include <iostream>
#include <utility>
#include <unordered_set>
#include <unordered_map>
#include <map>
#include <set>
#include <boost/functional/hash.hpp>
#include <boost/heap/fibonacci_heap.hpp>
#include "Eigen/SparseCore"

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImportImageFilter.h"
#include "itkImageFileWriter.h"

typedef short in_label;
typedef int label;
typedef itk::Image< in_label, 2 >         ImageType;

typedef itk::ImageFileReader< ImageType > ReaderType;

typedef itk::ImportImageFilter< in_label, 2 > ImportFilterType;

typedef std::pair< label, label > edge;
typedef double value;
//typedef std::unordered_map< edge, value, boost::hash<edge> > edge_map;
typedef std::map< edge, value > edge_map;



struct node_compare
{
   edge_map& m;

  node_compare(edge_map& m_) : m(m_) {} // std::cout << "construct " << this << " " << &m_ << " " << &m << " " << std::endl;}

  //  node_compare(const struct node_compare& o) : m(o.m) { std::cout << "copy construct " << this << " " << &o.m << " " << &m << " " << std::endl;}

    bool operator()(edge a, edge b) const
    {
      //      std::cout << "cmp " << this << " " << a << " " << b << " " << &m << std::endl; 
      //      std:: cout  << m.size() <<  std::endl;
      edge_map::iterator it1 = m.find(a), it2 = m.find(b);

        assert(it1 != m.end() && it2 != m.end());

        return it1->second < it2->second; 
	//      return m[a] > m[b];
    }
};
typedef boost::heap::fibonacci_heap< edge, boost::heap::compare<node_compare> > e_heap;
typedef typename e_heap::handle_type handle_t;
//typedef std::unordered_map<edge, handle_t, boost::hash<edge> > heap_map;
typedef std::map<edge, handle_t > heap_map;


class EQueue {
public:
  edge_map priorities;
  node_compare cmp;
  heap_map finder;
  e_heap heap;

  
  EQueue() :  heap(cmp), cmp(priorities) {} ;
  
  edge top() {
    return heap.top();
  }

  value top_weight() {
    return priorities[heap.top()];
  }
  
  void pop() {
    edge e = heap.top();
    //    std::cout << " pop " << e.first << " " << e.second << "\n";

    heap.pop();
    finder.erase(e);
    priorities.erase(e);
  }

  bool empty() {
    return heap.empty();
  }
  
  void insert(edge e, value w) {
    //std::cout << "insert " << e.first << " " << e.second << " " << w << "\n";
    priorities[e] = w;
    //std::cout << priorities.size() << " " << &priorities << std::endl;
    handle_t h = heap.push(e);
    finder[e] = h;
  }

  void remove(edge e) {
    //std::cout << "erase " << e.first << " " << e.second << "\n";
    auto it = finder.find(e);
    if(it != finder.end()) {
      handle_t h = it->second;
      //std::cout << "found " << (*h).first << " " << (*h).second << "\n";

      heap.erase(finder[e]);
      //      priorities.erase(e);
      finder.erase(e);
    } else {
      //std::cout << "************ not found \n";
    }
      
  }

  void update_priority(edge e, value w) {
    //std::cout << "update : " << e.first << " "  << e.second << " " << w << "\n";
    auto it = finder.find(e);
    if(it != finder.end()) {
      handle_t h = it->second;
      //std::cout << "found " << (*h).first << " " << (*h).second << "\n";
    
    //*h = e;
    priorities[e] = w;
    heap.update(h);
    }  else {
      //std::cout << "***********>>>>>>>>>>> not found \n";
    }
  }
};



typedef std::set< edge > edge_set;
typedef std::map< label, std::set<label> > graph;
//typedef std::unordered_map< label, int > area_map;
typedef std::map< label, double > area_map;


edge sort_pair(label a, label b)
{
  /*  
     Convert two integers into a sorted pair
  */
  if(a>b)
    return std::make_pair(b,a);
  else
    return std::make_pair(a,b);
}

template< typename T > void relabel(T* A, int N)
{
  std::set< T > l(A, A+N);
  std::map < T, T > m;
  int Nl = l.size();
  int i=0;
  for(typename std::set< T >::iterator it=l.begin(); it!=l.end(); ++i, ++it)
    {
      m[*it] = i;
    }
  for(int i=0; i<N; i++)
    A[i] = m[A[i]];
}

void superpixels(int* A, int N, short** B, int N_seg)
{
  short max_labels[N_seg];
  for(int i=0; i<N_seg; i++)
    {
      max_labels[i] = 0.0;
      for(int j=0; j<N; j++)
	{
	  max_labels[i] = std::max(max_labels[i], B[i][j]);
	}
    }
  int offsets[N_seg];
  offsets[0] = 1;
  for(int i=1; i<N_seg; i++)
    offsets[i] = offsets[i-1]*max_labels[i-1];
  for(int i=0; i<N; i++)
    {
      int v = 0;
      for(int j=0; j<N_seg; j++)
	v += B[j][i]*offsets[j];
      A[i] = v;
    }
  relabel(A, N);

  
  /*
    max_labels = [np.max(s) for s in segmentations]
    N_seg = len(max_labels)
    ml = np.cumprod(max_labels)
    ml2 = np.concatenate(([1],ml))
    ml = np.concatenate(([1],ml[:-1]))

    orig_l = np.sum(segmentations*ml[:, np.newaxis, np.newaxis, np.newaxis], axis=0)
    l, new_l = np.unique(orig_l, return_inverse=True)
    #label_map = np.array([(u%ml[1], (u%ml[2])/ml[1], u/ml[2]) for u in l])
#    label_map = np.array([(u%ml[1], (u%ml[2])/ml[1], u/ml[2]) for u in l])
    u = np.array(l)
    label_map = np.array([np.mod(u,ml2[i+1])/ml2[i] for i in range(N_seg)]).T
    return new_l.reshape(orig_l.shape), label_map
  */
}

edge_set make_connectivity_2d(label* A, int h, int w)
{
  /* 
      Make an unordered set conntaining all edges in the RAG
  */
  edge_set edges;
  /* Calculate matrix strides assuming no gaps due to row alignment */
  const int i_stride = w;
  const int j_stride = 1;
  // i-compare loop
    for(int i=0; i<h-1; i++)
      for(int j=0; j<w; j++)
	{
	  label idx1 = A[i*i_stride+j*j_stride];
	  label idx2 = A[(i+1)*i_stride+j*j_stride];
	    if(idx1 != idx2)
	      edges.insert(sort_pair(idx1, idx2));
	}
  // j-compare loop
  for(int i=0; i<h; i++)
    for(int j=0; j<w-1; j++)
	{
	  label idx1 = A[i*i_stride+j*j_stride];
	  label idx2 = A[i*i_stride+(j+1)*j_stride];
	    if(idx1 != idx2)
	      edges.insert(sort_pair(idx1, idx2));
	}
  return edges;
}

typedef Eigen::SparseMatrix<double> weight_type;
//typedef std::unordered_map<label, weight_type> label_weight_type;
typedef std::map<label, weight_type> label_weight_type;

label_weight_type convert_labels_weights(label* A, int N, in_label** B, int N_seg)
{
  /* Convert labels into sparse matrix */
  in_label N_val_max = 0;
  for(int j=0; j<N_seg; j++)
    {
      for(int i=0; i<N; i++)
	N_val_max = std::max(N_val_max, B[j][i]);
    }
  label_weight_type sp_weights;
  /* Loop over all pixels - add eacch pixel to appropriate weight matrix */
  /* The accumulation of matrix entries in Eigen::SparseMatrix may be somewhat
     inefficient */
  for(int i=0; i<N; i++)
    {
      label l = A[i];
      if(sp_weights.count(l)==0)
	{
	sp_weights[l] = weight_type(N_seg, N_val_max+1);
	//	std::cout << " > " << l << " " << &(sp_weights[l]) <<"\n";
	}
      for(int j=0; j<N_seg; j++)
	{
	  //	  std::cout << j << " " << B[j][i] << " " << N_seg << " " << N_val_max << "\n";
	  sp_weights[l].coeffRef(j, B[j][i]) += 1;
	}
    }
  //  for(auto x : sp_weights)
  ///  std::cout << "weight : " << x.first << " " << &(x.second) << "\n";
  return sp_weights;
}

area_map get_cell_areas(label* A, int N)
{
  area_map areas;
  for(int i=0; i<N; i++)
    areas[A[i]]+=1;
  return areas;
}

double multiply_sum(const weight_type& w0, const weight_type& w1)
{
  return (w0.cwiseProduct(w1)).sum();
}

double get_edge_weight(const label a, const label b, label_weight_type& sp_weights, area_map& sp_areas, const int N_seg)
{
  //  std::cout << sp_weights.size() << " ew : " << a << " " << b << std::endl;
  //  std::cout << sp_weights.size() << " " << sp_areas[a] << " " << sp_areas[b] <<" " << N_seg << std::endl;
  weight_type& w0 = sp_weights[a];
  weight_type& w1 = sp_weights[b];
  double m =  multiply_sum(w0, w1);
  double v = m/double(N_seg)/double(sp_areas[a])/double(sp_areas[b]);
  // std::cout << m << " " << v << "\n";
  return v;

}

graph get_graph_from_edges(edge_set edges)
{
  graph g;
  for(auto e : edges)
    {
      label a = e.first;
      label b = e.second;
      //std::cout << a << " " << b << "\n";
      g[a].insert(b);
      g[b].insert(a);
    }
  return g;
}

edge merge_graph(graph& g, label_weight_type& sp_weights, area_map& sp_areas, EQueue& edge_weights, int N_seg)
{
  edge e = edge_weights.top();
  edge_weights.pop();  
  label e0 = e.first;
  label e1 = e.second;
  sp_areas[e0] += sp_areas[e1];
  sp_areas.erase(e1);
  sp_weights[e0] += sp_weights[e1];
  sp_weights.erase(e1);
  

  std::set<label> edges_e0 = g[e0]; /* Presumably this gives a copy */
  std::set<label> edges_e1(g[e1].begin(), g[e1].end());

  std::set<label> updated_out_e0;
  for(label v : edges_e0)
    {
      if(e1!=v)
	updated_out_e0.insert(v);
    }

  std::set<label> new_out_e0;
  for(label v : edges_e1)
    {
      if (v!=e0)
	{
	  if ( (updated_out_e0.count(v)==0) /* && (new_out_e0.count(v)==0) */ )
	    {
	      new_out_e0.insert(v);
	    }
	}
    }

  for(label v: edges_e1)
    {
      /* Need to check if present ? */
      g[e1].erase(v); 
      g[v].erase(e1);
      /*
        k = sort_pair(e[1], v)
        if k in graph.queue:
	    del graph.queue[k]
      */
      if(v!=e0)
	edge_weights.remove(sort_pair(e1, v));
    }

  for(label v : updated_out_e0) {
    double w = get_edge_weight(e0, v, sp_weights, sp_areas, N_seg);
    edge_weights.update_priority(sort_pair(e0, v), w);
  }

  for(label v : new_out_e0) {
    double w = get_edge_weight(e0, v, sp_weights, sp_areas, N_seg);
    edge_weights.insert(sort_pair(e0, v), w);
    g[v].insert(e0);
    g[e0].insert(v);
  }
  return e;
}

int main(int argc, char **argv)
{

  int N_seg = argc-3;
  short* data[N_seg];
  ImageType::SizeType size;
  for(int i=0; i<N_seg; i++) {
    ReaderType::Pointer reader = ReaderType::New();
    reader->SetFileName(argv[i+3]);
    reader->Update();
    ImageType::Pointer im = reader->GetOutput();
    ImageType::RegionType region = im->GetLargestPossibleRegion();
    
    size = region.GetSize();

    std::cout << size << "\n";
    int N = size[0]*size[1];
    data[i] = new short[N];
    short* B = reader->GetOutput()->GetBufferPointer();
    std::copy(B, B+N, data[i]);
    relabel(data[i], N);
  }

  int N = size[0]*size[1];
    
  int A[N];
  superpixels(A, N, data, N_seg);

  
  //for(int i=0; i<N; i++)
  //  A[i] = int(data[0][i]);
  
  edge_set es = make_connectivity_2d(A, size[1], size[0]);

  for(auto e : es) {
    std::cout << e.first << " " << e.second << "\n";
  }
  
  area_map areas = get_cell_areas(A, N);
  for(auto e : areas) {
    std::cout << " a: " << e.first << " " << e.second << "\n";
  }
  label_weight_type weights = convert_labels_weights(A, N, data, N_seg);

  EQueue equeue;
  for(auto e: es) {
    value w = get_edge_weight(e.first, e.second, weights, areas, N_seg);
    equeue.insert(e,w);
  }

  graph g = get_graph_from_edges(es);

  double threshold = std::strtod(argv[2], 0);
  
  std::map<label, label> mapping;
  while(!(equeue.empty()) && (equeue.top_weight()>threshold) ) {
    edge e = equeue.top();
    std::cout << "merge " << equeue.top_weight() << " " << e.first << " " << e.second << "\n";
    merge_graph(g, weights, areas, equeue, N_seg);
    mapping[e.second] = e.first;
  }

  in_label A_out[N];

  
  for(int i=0; i<N; i++)
    {
      short v = A[i];
      while(true) {
	std::map<label, label>::const_iterator it = mapping.find( v );
	if (it != mapping.end()) {
	  v = it->second;
	} else {
	  break;
	}
      }
      A_out[i] = short(v);
    }
  /* Write image via ITK */


  ImportFilterType::Pointer importFilter = ImportFilterType::New();
  
  ImportFilterType::IndexType start;
  start.Fill( 0 );
  ImportFilterType::RegionType region;
  region.SetIndex( start );
  region.SetSize( size );
  importFilter->SetRegion( region );

  const itk::SpacePrecisionType origin[2] = { 0.0, 0.0 };
  importFilter->SetOrigin( origin );

  const itk::SpacePrecisionType spacing[2] = { 1.0, 1.0 };
  importFilter->SetSpacing( spacing );

  importFilter->SetImportPointer( A_out, N, false );

  // Finally, we can connect the output of this filter to a pipeline.
  // For simplicity we just use a writer here, but it could be any other filter.
  using WriterType = itk::ImageFileWriter< ImageType >;
  WriterType::Pointer writer = WriterType::New();

  writer->SetFileName( argv[1] );
  writer->SetInput(  importFilter->GetOutput()  );
    try
      {
	writer->Update();
      }
    catch( itk::ExceptionObject & exp )
      {
	std::cerr << "Exception caught !" << std::endl;
	std::cerr << exp << std::endl;
	return EXIT_FAILURE;
      }
  
  
}

