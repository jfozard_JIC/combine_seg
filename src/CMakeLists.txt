set (CMAKE_CXX_STANDARD 11)
set (CMAKE_BUILD_TYPE Release)
project(seg_combine)

find_package(Eigen3 REQUIRED)
find_package(ITK REQUIRED)
include(${ITK_USE_FILE})
include_directories(${EIGEN3_INCLUDE_DIR})
add_executable(seg_combine3d combine_pq_3d.cpp)
add_executable(seg_combine2d combine_pq_2d.cpp)
target_link_libraries(seg_combine2d ${ITK_LIBRARIES})
target_link_libraries(seg_combine3d ${ITK_LIBRARIES})
